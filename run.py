#!/usr/bin/env python3.9
from ytdweb.settings import _CERT_LOCATION_, SECRET_KEY

ssl         = False
gencert     = False
gensecret   = False
servset     = False
addr        = '0.0.0.0'
port        = '8000'

def secret_generator(prefix : str = 'ytdivert-', suffix : str = '.vasilaev', length : int = 128):
    from random import randint
    chars = 'abcdefghijklmnopqrstuvwxyz' \
            'ABCDEFGHIJKLMNOPQRSTUVXYZ' \
            '0123456789' \
            '()^[]-$#_+!'
    retval = prefix
    for i in range(len(prefix),length - len(suffix)):
        retval += chars[randint(0,len(chars) - 1)]
    return retval + suffix

def argproc():
    global ssl, gencert, servset, addr, port, gensecret
    from sys import argv
    for arg in argv:
        if (arg    == "ssl") and (not gencert) and (not gensecret):
            ssl     = True
            if not servset:
              port  = '443'
        if arg     == "gencert":
            gencert = True
        if arg     == 'gensecret':
          gensecret = True
        if arg      . count(':') > 0:
            if arg.startswith(':'):
                port = arg.removeprefix(':')
            elif arg.endswith(':'):
                addr = arg.removesuffix(':')
            else:
                addr    = arg.split(':')[0]
                port    = arg.split(':')[1]
            servset = True
        

def main():
    """
    """
    argproc()
    import shutil, os, sys
    from ytdweb.settings import DATABASES, YTD_SALT
    from ytdapp.ytd import path_combine, SCRAPER_DIR
    from hashlib import sha256, sha512
    from getpass import getpass
    global ssl, gencert, servset, addr, port, gensecret
    
    if gencert:
        if os.name == 'nt':
            if shutil.which("openssl.exe"):
                _code = os.system('openssl.exe req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ytdweb/priv/privkey.pem -out ytdweb/priv/cert.pem')
                if _code == 0:
                    print("")
                    print("===========================")
                    print(f"Successfully Generated Cert & Key in '{os.path.abspath('ytdweb/priv')}'.")
                    print("")
                    print("Now terminating program.")
                else:
                    print("")
                    print("===========================")
                    print(f"Something went wrong with openssl, return code {_code}.")
                    print("Now terminating program.")
                exit(_code)
            else:
                print('A valid installation of openssl for Windows is required to generate a cert and key. We recommend https://indy.fulgan.com/SSL/')
                exit(300)
        else:
            if shutil.which("openssl"):
                _code = os.system('openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ytdweb/priv/privkey.pem -out ytdweb/priv/cert.pem')
                if _code == 0:
                    print("")
                    print("===========================")
                    print(f"Successfully Generated Cert & Key in '{os.path.abspath('ytdweb/priv')}'.")
                    print("")
                    print("Now terminating program.")
                else:
                    print("")
                    print("===========================")
                    print(f"Something went wrong with openssl, return code {_code}.")
                    print("Now terminating program.")
                exit(_code)
            else:
                print('A valid installation of openssl is required to generate a cert and key.')
                exit(300)
    elif gensecret:
        newFile = ''
        with open('ytdweb/settings.py', 'r') as settings:
            bak = ''
            for line in settings:
                bak += line
                if line.count("SECRET_KEY = ") > 0:
                    newFile += f"""SECRET_KEY = '{secret_generator()}'\n"""
                else:
                    newFile += line
        settin = open('ytdweb/settings.py', 'w')
        settin.write(newFile)
    elif (not (len(SECRET_KEY) > 1)) and (not (SECRET_KEY.count('django-'))):
        print('No secret key installed. Run \'run.py gensecret\' to continue.')
        return 104
    elif os.path.exists(DATABASES['default']['NAME']):
        apps_pass = False
        if (os.name == 'nt'):
            loc    = os.path.abspath('ytdapp')
            
            if os.path.isfile(path_combine(loc, "mpv/mpv.exe")) or shutil.which('mpv.exe'):
                if os.path.isfile(path_combine(loc, "yt-dlp/yt-dlp.exe")) or shutil.which('mpv.exe'):
                    apps_pass = True
                else:
                    print(f"Unable to find yt-dlp at location: '{path_combine(loc, 'yt-dlp.exe')}'.")
                    return(2)
            else:
                print(f"Unable to find mpv at location: '{path_combine(loc, 'mpv.exe')}'.")
                return(1)
        else:
            if os.path.isfile(path_combine(loc, "mpv")) or shutil.which("mpv"):
                if os.path.isfile(path_combine(loc, "yt-dlp")) or shutil.which("mpv"):
                    apps_pass = True
                else:
                    print(f"Unable to find yt-dlp: '{path_combine(loc, 'yt-dlp')}'.")
                    re4turn(2)
            else:
                print("Unable to find mpv. Make sure it is installed and its executable binary file is in $PATH.")
                return(1)
        if apps_pass:
            import sqlite3
            con         = sqlite3.connect(DATABASES['default']['NAME'])
            cur         = con.cursor()
            cur         . execute("SELECT name FROM sqlite_master WHERE type='table' AND name='ytd_auth';")
            need_user   = False
            need_table  = False
            _c          = cur.fetchone()
            print(_c)
            if _c:
                cur     . execute("SELECT Username FROM ytd_auth;")
                if not (cur.fetchone()):
                    need_user = True
            else:
                need_table  = True
            if need_table or need_user:
                
                print("""A master YTDivert user has not been detected.\n
                Please create a new master user by following the prompts below:""")
                print("")
                user        = ""
                userok      = False
                while (len(user) < 4)and(not userok):
                    print("""Username Criteria:\n
                     - Greater than 3 characters in length\n
                     - Must be alphanumeric (plus underscore)""")
                    print("")
                    user    = input("Enter a username: ")
                    import    re
                    charRe  = re.compile(r'[^a-zA-Z0-9\_]')
                    userok  = not bool  (charRe.search(user))
                passwd      = ""
                while (len(passwd) < 8):
                    print               ("""Password Criteria:
                     - Greater than 7 characters in length
                     - All characters supported""")
                    print               ("")
                    passwd  = getpass   ("Enter a password: ")
                usersha     = sha256((user   + YTD_SALT).encode('utf-8')).hexdigest()
                passwdsha   = sha512((passwd + YTD_SALT).encode('utf-8')).hexdigest()
                del user, passwd
                if need_table:
                    cur     . execute("CREATE TABLE \"ytd_auth\" ( \"Username\" TEXT UNIQUE, \"Password\" TEXT, PRIMARY KEY(\"Username\") );")
                cur         . execute(f"INSERT INTO \"ytd_auth\" VALUES ('{usersha}','{passwdsha}');")
                con         . commit()
                con         . close()
                return(main())
        else:
            print("""Missing installation of mpv or yt-dlp. Please install for your system (according to README) to continue.""")
    else:
        print("""
        YTDivert SQLite3 Database not found.
        Please run server using 'manage.py runserver' to create a new database, then rebase the databse by executing 'manage.py migrate'.
        See Django documentation for more information.""")
        return(1)
    
    if not (gencert or gensecret):
        runstr = 'python3.9 manage.py run'
        if ssl:
            if os.path.isfile(_CERT_LOCATION_ + "/cert.pem") and os.path.isfile(_CERT_LOCATION_ + "/cert.pem"):
                runstr += (f"sslserver {addr}:{port} --certificate {_CERT_LOCATION_}/cert.pem --key {_CERT_LOCATION_}/privkey.pem")
            else:
                print(f"""Could not find the cert or key file in {_CERT_LOCATION_}.""")
                return(20)
        else: 
            runstr += ("server {addr}:{port}")
        return(os.system(runstr))
    else:
        return(0)


if __name__ == "__main__":
    code = main()
    print(f"Returned {code}")
    exit(code)
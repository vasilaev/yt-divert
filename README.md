# yt-divert

Youtube Divert (yt-divert) is a free, FOSS python-php webapp that scrapes and plays YouTube videos via a web server without the need for the YouTube API v3 or any of that dog-water tracking software. This project uses yt-video-list python library as well as mpv, yt-dlp, and django to act as a pseudo-YouTube.

## WARNING

There may or may not be unknown security flaws with this project. You have been warned.

## Features

#### Cross-Platform
Because this project is made using purely python, it is capable of running X-platform.

#### Basically no javascript
If you exclude the scraping process (which caches no data and accepts no cookies), there is practically no javascript other than a feew lines for proper PHP functionality (and displaying year).

#### NO YOUTUBE API
I hate having to tell google what I watch! Why should I have to use the API? Well here you don't have to! Sure you sacrifice some of your CPU's lifetime, but that's only really for scraping new channels

#### Updating is less painful than fresh scraping
Updating doesn't take forever if you already have the channel scraped!

#### Automatic file management of scraped files
^^^ Self Explanatory ^^^

#### Web Interface
All components can be modified and accessed through the web interface. This includes updating channels, adding/removing channels, accessing channels, and watching videos.

#### Made with FOSS
FOSS is boss, and this project is Free as in Freedom.

# Getting started

Using this package should be fairly straightforward, but there are some requirements that must be met first:

## Dependencies

- PC is running >=Windows 7 or >=Linux 4.15
- Minimum 8GB of system RAM to prevent paging, excessive swap usage, or memory-related crashes.
- A Webserver with >=PHP8
- Python 3.9+ with yt-videos-list library
- MPV & yt-dlp

## Installation

Installation is not necessarily the easiest, but it is easier to install than many, many C libraries. Sudo or cmd admin privileges may be necessary to install everything properly.

#### Dependencies
1. Download & install python/pip. If On Windows: download [python 3.9.10](https://www.python.org/ftp/python/3.9.10/python-3.9.10-amd64.exe). If On Linux:
```
apt-get install python3.9 -y
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.9 get-pip.py
rm get-pip.py
```
You *may* need to add pip3.9 to path. The pip installation may tell you that pip is not in PATH. Copy that directory and perform this command where [INSERT HERE] is the directory that the script prints (may look something like `/home/user/.local/bin`):
```
PATH="$PATH:[INSERT HERE]"
    --- May Look Like: ---
PATH="$PATH:/home/user/.local/bin"
```

2. Use pip to get yt-videos-list, django, django-sslserver, and django-session-timeout:
```
pip3.9 install yt-videos-list django django-sslserver django-session-timeout
```

3. Either:
  - In a directory of your choice, create a new webserver root directory for django (See *Step 1* of **Setup**), then create a python venv inside it. OR
  - Ensure django executables are inside your PATH environment variable (typically inside `scripts/` where python resides)

#### Setup

1. Download the latest version off the git repo and copy them to a secure, organized location that will hold your python webserver:

```
mkdir /new/webserver/root/dir/
cd /new/webserver/root/dir/
git clone https://gitlab.com/vasilaev/yt-divert.git
```

2. Get [MPV](https://mpv.io/installation/) for your OS. For windows, download and place the following files/folders in the `ytdapp/mpv` folder in your webserver root:

```
/mpv
d3dcompiler_43.dll
mpv.com
mpv.exe
```

3. Get [yt-dlp](https://github.com/yt-dlp/yt-dlp/releases/) for your OS. For windows, download and place `yt-dlp.exe` in the `ytdapp/yt-dlp` folder in your webserver root.

4. Get your favorite Selenium driver executable (preferably [geckodriver](https://github.com/mozilla/geckodriver/releases) because it is default), then install it in the `ytdapp/scraper/driver` folder in your webserver root.

5. [OPTIONAL][GOOD FOR SECURITY] Obtain a new self-signed cert by deleting the certs in the `ytdweb/priv` folder in the webserver root, then download openssl for your OS and run the following commands:

```
cd /new/webserver/root/dir/yt-divert/
python3.9 ./run.py gencert
```

If openssl is installed, it will generate a key for you, otherwise it will fault out. An alternative would be to do:

```
cd /new/webserver/root/dir/yt-divert/
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ytdweb/priv/privkey.pem -out ytdweb/priv/cert.pem
```

6. Generate a new SECRET_KEY (REQUIRED) by running the following from your webserver root:

```
python3.9 ./run.py gensecret
```

7. Run the following to begin the webserver in HTTP or HTTPS mode with the `ssl` option:

```
python3.9 ./run.py         -- HTTP  on port 8000
python3.9 ./run.py ssl     -- HTTPS on port 443
```

**HEADS UP**: Don't use the ./run.py command anywhere outside of the yt-divert directory in your webserver directory. It relies on this being consistant.

8. Open `[http/https]://[HOSTNAME]/` on your browser (where HOSTNAME is a URL under ALLOWED_HOSTS). Login when prompted.
9. Click the edit subscriptions button at the top of the screen, then follow what the document states in order to properly add new channels.
10. Save Changes then go back and press the update button at the top of the screen. Let the program update, this may take a very, very long time depending on how many channels are listed as well as the number of videos per channel. A channel with 3000 videos could take 10 minutes or more alone. If the app is updating, it will take significantly less. Maybe 5 minutes or less.
11. Click on a channel, click a video, and enjoy!

#### Extra Functionality

`python3.9 ./run.py`                          - Run django HTTP webserver on port 8000 with no ssl.
`python3.9 ./run.py address:port`             - Run django HTTP webserver on specified address and port.
`python3.9 ./run.py :port`                    - Run django HTTP webserver on localhost over specified port.
`python3.9 ./run.py address:`                 - Run django HTTP webserver on specified address on default port.
`python3.9 ./run.py ssl`                      - Run django HTTPS webserver on port 443 with ssl.
`python3.9 ./run.py ssl address:port`         - Run django HTTPS webserver on specified address and port with ssl.
`python3.9 ./run.py gencert`                  - Generates ssl certificate and private key using OpenSSL
`python3.9 ./run.py gensecret`                - Generates SECRET_KEY for django webserver

# Future Milestones & Goals
This project is loosely maintained as I, the project maintainer, am a school-going and working individual. Hopefully this project does not get lost, but the goal is to make changes at least once a month.

## Long Term
- [x] Set up Web Interface for yt-video-list library
- [x] Set up linux support!!
  * [x] Convert Bat to Py
  * [x] Set up auto x-platform mpv exec
  * [x] Set up x-platform drivers
  * [x] Make x-platform README
- [x] Make more secure!!
  * [x] Filter commands through ~~dictionary?~~ multiple functions.
  * [x] ~~Remove all calls to exec(); or similar~~
    + REMOVED
  * [x] ~~Run escapeshellcmd and escapeshellarg before exec(); calls.~~
    + ADDED
  * [x] Tokenize requests (Client & Server)
  * [x] ~~Encrypt HTTP(S) data before POST or GET (Client & Server)~~ Replaced with login form
    + REMOVED
  * [x] ~~GET RID OF SELENIUM?? (Might be longer term)~~ moved to longer term
    + MOVED FROM
  * [x] Create login form
    + ADDED
- [x] Let client know when updating is complete.
- [x] Better filesystem management & organization
- [ ] Create Edit Channel Page (Not Edit Subscriptions)
- [ ] Add Channel Descriptions from Subs File
- [ ] Create an installer
    + MOVED TO
- [ ] Add Remote MPV Capability

## Longer Term
- [ ] Make less reliant on dependencies
- [x] Make from-scratch webpages
  * [x] Make code seem less obfuscated, organize PHP better
  * [x] PHP -> Python? Python Webserver?
  * [x] Make more user-friendly
- [x] Optimize Python scripts
- [ ] ~~Create an installer~~ shorter term
    + MOVED FROM
- [ ] Perform auto channel updates? Maybe just have cron do it.
- [ ] Get rid of Selenium
    + MOVED TO

# License
This project is and always will be Free as in Freedom. Open Source under the GNU GPLv3 Affero as well as other accompanying Open Source Licenses. See [License](https://gitlab.com/vasilaev/yt-divert/-/blob/main/LICENSE) for more details.

# Author
Created By Vasil Nikolaev
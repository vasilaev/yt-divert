from typing import Literal
from django.shortcuts import redirect
from django.http import HttpResponse, HttpRequest
from ytdapp.ytd import *

__pageHeaderIO__    = open('ytdapp/template/header.html', 'r')
pageHeader          = __pageHeaderIO__.read()
__pageHeaderIO__    . close()

__ERROR_DICT__      = {
    400             : ("Bad Request",           "The server cannot or will not process the request."),
    401             : ("Unauthorized",          "The client does not have access to this resource."),
    403             : ("Forbidden",             "The server refuses to provide the requested resource."),
    404             : ("Not Found",             "The server cannot find the requested resource, view, or script."),
    405             : ("Method Not Allowed",    "The requested method is known, but not supported by the target resource."),
    406             : ("Not Acceptable",        "No acceptable content for the user-agent."),
    407             : ("Proxy Auth Required",   "The client, by proxy, does not have access to this resource."),
    408             : ("Request Timeout",       "The server has timed out the response to the request."),
    409             : ("Conflict",              "Request and current state currently conflict."),
    410             : ("Gone",                  "Resource has been permanently deleted from server."),
    413             : ("Payload Too Large",     "Request entity is larger than server max size."),
    414             : ("URI Too Long",          "URI is too long for server."),
    415             : ("Unsupported Media",     "Server does not support media type."),
    416             : ("Range Not Satisfiable", "The Range specifier in the header cannt be fulfilled."),
    417             : ("Expectation Failed",    "The Expect header field cannot be fulfilled by the server."),
    418             : ("I'm A Teapot",          "You head it here first, folks."),
    429             : ("Too Many Requests",     "The user agent has sent too many requests."),
    500             : ("Internal Server Error", "The Server has encountered a situation it doesn't know how to handle."),
    501             : ("Not Implemented",       "No server-side implementation for the request method."),
    502             : ("Bad Gateway",           "The server got an invalid response while working as a gateway to get response."),
    503             : ("Service Unavailable",   "The server is not ready to handle the request."),
    504             : ("Gateway Timeout",       "The server timed out while working as a gateway to get response."),
    505             : ("HTTP Ver. No Support",  "The HTTP version in the request cannot be handled."),
    510             : ("Not Extended",          "Further extensions to the request are required for the server to fulfill it."),
    511             : ("Network Auth Required", "The client needs to authenticate to gain network access.")
}
"""Dictionary that contains most HTTP/WebDAV Errors and their meanings."""

# Create your views here.
def login(request : HttpRequest, message : str = "", redir : str = "channels"):
    from hashlib import md5
    _a = auth(request)
    if request.is_secure():
        redir = "https://" + request.get_host() + '/' + redir
    else:
        redir = "http://" + request.get_host() + '/' + redir
    if _a == 0:
        return redirect(redir)
    elif _a == 2:
        message = "Credentials Incorrect. Try Again."
    elif _a == 3:
        message = "Session Tampered. Retry Later."
    from random import randint
    from ytdweb.settings import YTD_SALT
    template                 = 'login.html'
    context                  = {
        "msg"                : f"{message}"
    }
    from django.shortcuts import render
    return render(request, template, context)
        

def channels(request : HttpRequest):
    if not auth(request) == 0:
        return login(request)
    else:
        text = """"""
        subsPageIO  = open('ytdapp/template/subs.html', 'r')
        subsPage    = subsPageIO.read()
        subsPageIO             .close()
        for _s in getchs():
            text += f"'{_s}', "
        subsPage    = subsPage.replace("/*let data = ;*/", f"""let data = [ {text} ];""")
        return HttpResponse(pageHeader + subsPage)

def videos(request : HttpRequest):
    if not auth(request) == 0:
        return login(request, request.get_full_path(False))
    else:
        if request.GET.get('c'):
            chan = request.GET.get('c')
            import    re
            charRe  = re.compile(r'[^a-zA-Z0-9-_.]')
            chanok  = not bool  (charRe.search(chan))
            if chanok:
                videosPageIO    = open('ytdapp/template/channel.html', 'r')
                subVideosIO     = open(f'{OUTPUT_DIR}/{chan}', 'r')
                videosPage = videosPageIO.read()
                subVideos = subVideosIO.read()
                videosPageIO.close()
                subVideosIO.close()
                videosPage      = videosPage.replace("/*let data = ;*/", f"""let data = [ {subVideos} ];""").replace('<!-- ChannelName -->', chan)
                return HttpResponse(pageHeader + videosPage)
            else:
                print('Attempted injection: ?c=' + request.GET.get('c'))
                return redirect('channels')
        else:
            return redirect('channels')

def repair(request : HttpRequest):
    if not auth(request) == 0:
        return login(request, request.get_full_path(False))
    else:
        import ytdapp.ytd as ytd
        ytd.repair()
        return redirect('channels')

def update(request : HttpRequest):
    if not auth(request) == 0:
        return login(request, request.get_full_path(False))
    else:
        from _thread import start_new_thread
        start_new_thread(run, ())
        return redirect('updating')

def updating(request : HttpRequest):
    if not auth(request) == 0:
        return login(request, request.get_full_path(False))
    else:
        global Scraper
        if Scraper.Updating and not Scraper.UpdatingDN:
            updatingPageIO  = open('ytdapp/template/update.html')
            updatingPage    = updatingPageIO.read()
            updatingPageIO  .close()
            return HttpResponse(pageHeader + updatingPage)
        elif not Scraper.Updating and Scraper.UpdatingDN:
            Scraper.UpdatingDN = False
            return redirect('channels')
        else:
            return redirect('channels')
            
def watch(request : HttpRequest):
    if not auth(request) == 0:
        return login(request, request.get_full_path(False))
    else:
        if request.GET.get('v'):
            mpvWatchYT(request.GET.get('v'))
            watchingIO          = open('ytdapp/template/watch.html')
            watchingPage        = watchingIO.read()
            watchingIO          . close()
            sender = ""
            if request.GET.get('sender'):
                sender          = f"/videos?c={request.GET.get('sender')}"
                watchingPage    = watchingPage.replace('<a href="/channels"><< Go Back</a>', f'<a href="{sender}"><< Go Back</a>').replace('window.location.href = \'/channels\';', 'window.location.href = \'' + sender + '\';')
            return HttpResponse(pageHeader + watchingPage)

def edit(request : HttpRequest):
    if not auth(request) == 0:
        return login(request, request.get_full_path(False))
    else:
        from ytdapp.ytd       import SUBS_FILE
        msg   : str = ""
        subscriptionsIO              = open(SUBS_FILE, 'r')
        subs                         = subscriptionsIO.read()
        subscriptionsIO              . close()
        if request.POST              . get('content'):
            subs                     = request.POST.get('content')
            subscriptionsIO          = open(SUBS_FILE, 'w')
            subscriptionsIO          . write(subs)
            subscriptionsIO          . close()
        template                     = 'edit.html'
        context                      = {
            "file_contents"          : f"{subs}",
            "msg"                    : f"{msg}"
        }
        from django.shortcuts import render
        return render(request, template, context)




# HTTP Error Pages
from django.shortcuts import render
def __gen_err__(code : int):
        template         = 'err.html'
        context          = {
            "error_code" : f"{str(code)}, {__ERROR_DICT__[code][0]}",
            "error_msg"  : f"{__ERROR_DICT__[code][1]}"
        }
        return(template,context)

def err400(request : HttpRequest, exception): errNum = 400; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err401(request : HttpRequest, exception): errNum = 401; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err403(request : HttpRequest, exception): errNum = 403; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err404(request : HttpRequest, exception): errNum = 404; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err405(request : HttpRequest, exception): errNum = 405; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err406(request : HttpRequest, exception): errNum = 406; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err407(request : HttpRequest, exception): errNum = 407; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err408(request : HttpRequest, exception): errNum = 408; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err409(request : HttpRequest, exception): errNum = 409; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err410(request : HttpRequest, exception): errNum = 410; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err413(request : HttpRequest, exception): errNum = 413; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err414(request : HttpRequest, exception): errNum = 414; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err415(request : HttpRequest, exception): errNum = 415; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err416(request : HttpRequest, exception): errNum = 416; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err417(request : HttpRequest, exception): errNum = 417; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err418(request : HttpRequest, exception): errNum = 418; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err429(request : HttpRequest, exception): errNum = 429; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err500(request : HttpRequest)           : errNum = 500; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err501(request : HttpRequest)           : errNum = 501; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err502(request : HttpRequest)           : errNum = 502; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err503(request : HttpRequest)           : errNum = 503; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err504(request : HttpRequest)           : errNum = 504; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err505(request : HttpRequest)           : errNum = 505; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err510(request : HttpRequest)           : errNum = 510; err = __gen_err__(errNum); return render(request, err[0], err[1])
def err511(request : HttpRequest)           : errNum = 511; err = __gen_err__(errNum); return render(request, err[0], err[1])
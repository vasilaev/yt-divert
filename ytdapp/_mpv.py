#!/bin/bash python3.9

def main(override : bool = False, location : str = '', _file : str = ''):
    from sys import argv
    import os, shutil
    from ytdapp.ytd import path_combine
    file : str = ''
    loc  : str = ''
    from subprocess import Popen
    if override:
        loc = location
        file = _file
    else:
        if (argv[0].count(".py") > 0) or (argv[0].count("mpv.py") > 0):
            file = argv[1]
            if len(argv) > 2: loc  = argv[2]
        else:
            file = argv[0]
            if len(argv) > 1: loc  = argv[1]
    if (os.name == 'nt'):
        if shutil.which(path_combine(loc, "mpv/mpv.exe")):
            if shutil.which(path_combine(loc, "yt-dlp/yt-dlp.exe")):
                Popen(f'{path_combine(loc, "mpv/mpv.exe")} --script-opts=ytdl_hook-ytdl_path={path_combine(loc, "yt-dlp/yt-dlp.exe")} {file}')
            else:
                print(f"Unable to find yt-dlp at location: '{path_combine(loc, 'yt-dlp/yt-dlp.exe')}'.")
                return(2)
        else:
            print(f"Unable to find mpv at location: '{path_combine(loc, 'mpv/mpv.exe')}'.")
            return(1)
    else:
        if shutil.which("mpv"):
            if shutil.which(path_combine(loc, "yt-dlp")):
                Popen(f'mpv --script-opts=ytdl_hook-ytdl_path={path_combine(loc, "yt-dlp")} {file}')
            else:
                print(f"Unable to find yt-dlp: '{path_combine(loc, 'yt-dlp')}'.")
                return(2)
        else:
            print("Unable to find mpv. Make sure it is installed and its executable binary file is in $PATH.")
            return(1)
    return(0)

if __name__ == "__main__":
    exit(main())

WEB_DRIVER      = f"""firefox"""
"""Selected web driver."""
from io import TextIOWrapper
import os, shutil
from tracemalloc import start
WEB_DRIVER_DIR  = f"""{os.path.abspath('ytdapp/scraper/driver')}"""
"""Directory for Selenium web driver."""
SCRAPER_DIR     = f"""{os.path.abspath('.')}"""
"""Directory for scraper files."""
OUTPUT_DIR      = f"""{os.path.abspath('ytdapp/scraper')}"""
"""Cleaned output for scraper."""
GENERATION_DIR  = f"""{os.path.abspath('ytdapp/cache')}"""
"""Directory for generated channel cache."""
SUBS_FILE       = f"""{os.path.abspath('ytdapp/scraper/subscriptions')}"""
"""Subscriptions file."""

LOG_SILENTLY    = False
"""Runs Selenium Driver with no logs."""
RUN_HIDDEN      = True
"""Runs Selenium Driver hidden if possible."""
CHANNEL_UPDATE  = True
"""Runs update routine for regular scraping."""
SPECIAL_UPDATE  = True
"""Runs update routine for FixedID scraping."""
UNSAFE          = False
"""Runs without try statements."""

THREADS         = 4
"""Number of threads to run selenium scraping process with."""
MIN_SLEEP_DUR   = 4
"""Minimum sleep duration between scrapes in seconds."""
MAX_SLEEP_DUR   = 7
"""Maximum sleep duration between scrapes in seconds."""
FORCE_STOP_CHN  = 20
"""Force sleep after this many channels."""
FORCE_STOP_DUR  = 10
"""Force sleep duration in seconds."""

WEB_DRIVER_DICT = {
  'firefox'     : 'geckodriver',
  'chrome'      : 'chromedriver',
  'opera'       : 'operadriver',
  'brave'       : 'bravedriver',
}
"""Dictionary that helps associate browser name with executable."""

from asyncio import sleep
import ytdweb.settings as ytdsettings
import yt_videos_list as ytvl
from django.http        import HttpRequest

import os

def walklevel(path, depth = 1):
    """It works just like os.walk, but you can pass it a level parameter
       that indicates how deep the recursion will go.
       If depth is 1, the current directory is listed.
       If depth is 0, nothing is returned.
       If depth is -1 (or less than 0), the full depth is walked.
    """
    # If depth is negative, just walk
    # Not using yield from for python2 compat
    # and copy dirs to keep consistant behavior for depth = -1 and depth = inf
    if depth < 0:
        for root, dirs, files in os.walk(path):
            yield root, dirs[:], files
        return
    elif depth == 0:
        return

    # path.count(os.path.sep) is safe because
    # - On Windows "\\" is never allowed in the name of a file or directory
    # - On UNIX "/" is never allowed in the name of a file or directory
    # - On MacOS a literal "/" is quitely translated to a ":" so it is still
    #   safe to count "/".
    base_depth = path.rstrip(os.path.sep).count(os.path.sep)
    for root, dirs, files in os.walk(path):
        yield root, dirs[:], files
        cur_depth = root.count(os.path.sep)
        if base_depth + depth <= cur_depth:
            del dirs[:]

def millis() -> int:
    from time import time; return round(time() * 1000)

def auth(request : HttpRequest):
    """Authorizes access to pages, returns a code to indicate authorization state.

    Args:
    >>> request(HttpRequest): "The request being handled."

    Returns:\n
        int:
    >>> 0 = "Success"
    >>> 1 = "No Attempt"
    >>> 2 = "Unsuccessful Attempt"
        
    """
    import sqlite3
    from hashlib import sha512, sha256
    from ytdweb.settings import DATABASES, YTD_SALT
    con = sqlite3.connect(DATABASES['default']['NAME'])
    cur = con.cursor()
    cur.execute('SELECT Username FROM ytd_auth;')
    if request.session.get('session_id').__str__() == list(cur.fetchone())[0]:
        con.close()
        return 0
    else:
        if request.POST.get('user') and request.POST.get('pass'):
            usersha                             = sha256((request.POST['user'] + YTD_SALT).encode('utf-8')).hexdigest()
            passsha                             = sha512((request.POST['pass'] + YTD_SALT).encode('utf-8')).hexdigest()
            cur                                 . execute(f"SELECT Username FROM ytd_auth WHERE Username='{usersha}' AND Password='{passsha}';")
            if cur.fetchone():
                request.session['session_id']   = usersha
                con                             . close()
                return 0
            else:
                con                             . close()
                return 2
        else:
            con                                 . close()
            return 1

def parse_boolstr(string : str) -> bool:
  """Convert a Boolean String to Bool

  Args:
      string (str): String to convert.

  Raises:
      Exception: Input is not in boolean format.

  Returns:
      bool
  """
  if (string.lower() == 'true') or (string.lower() == 'yes'):
    return True
  elif (string.lower() == 'false') or (string.lower() == 'no'):
    return False
  raise Exception("Input '" + string + "' is not in boolean format.")

def parse_tuplestr(string : str, __type__ : type = type(int)) -> tuple:
  """Convert a Tuple String to a Tuple.

  Args:
      string (str): String to convert
      __type__ (type, optional): Type to convert to. Defaults to type(int).

  Raises:
      Exception: Unsupported type (__type__)
      Exception: Input is not in tuple format (string)

  Returns:
      tuple: Tuple with values of type __type__.
  """
  if string.startswith("("):
    if (string.count(",") == 1) and (string.count("(") == 1) and (string.count(")") == 1):
      if string.endswith(")"):
        newstr = string.removeprefix("(").removesuffix(")").split(",",1)
        if   __type__ == type(int):
          return (int(newstr[0]),int(newstr[1]))
        elif __type__ == type(float):
          return (float(newstr[0]),float(newstr[1]))
        elif __type__ == type(bool):
          return (parse_boolstr(newstr[0]),parse_boolstr(newstr[1]))
        else:
          raise Exception("Unsupported type: " + __type__)
  raise Exception("Input '" + string + "' is not in tuple format.")

def __f_pth_arg_com_(path1 : str, path2 : str) -> str:
  import os
  if (os.name == 'nt'):
    retpath1 : str
    retpath2 : str
    if (path1 == '') or (path1 == None):
        return path2
    if path1.endswith("\\"):
      retpath1 = path1.replace('/', '\\')
      retpath2 = path2.replace('/', '\\')
      if retpath2.startswith('\\'):
        return retpath1 + retpath2.removeprefix('\\')
      return retpath1 + retpath2
    elif path1.endswith('/'):
      retpath1 = path1.replace('\\', '/')
      retpath2 = path2.replace('\\', '/')
      if retpath2.startswith('/'):
        return retpath1 + retpath2.removeprefix('/')
      return retpath1 + retpath2
    else:
      if path2.startswith('/') or path2.startswith('\\'):
        if path1.count('\\') > path1.count('/'):
          retpath1 = path1.replace('/', '\\')
          retpath2 = path2.replace('/', '\\')
        else:
          retpath1 = path1.replace('\\', '/')
          retpath2 = path2.replace('\\', '/')
        return retpath1 + retpath2
      else:
        if path1.count('\\') > path1.count('/'):
          retpath1 = path1.replace('/', '\\')
          retpath2 = path2.replace('/', '\\')
          return retpath1 + '\\' + retpath2
        else:
          retpath1 = path1.replace('\\', '/')
          retpath2 = path2.replace('\\', '/')
          return retpath1 + '/' + retpath2
  else:
    if path1.endswith('/'):
      if retpath2.startswith('/'):
        return path1 + path2.removeprefix('/')
      return path1 + path2
    else:
      if path2.startswith('/'):
        return path1 + path2
      return path1 + '/' + path2
        
def path_combine(*args):
  """Combine paths together in the form of multiple strings, lists, or tuples. Mixing and matching types is welcome. Operation is performed left to right with NT formatting.

  ```Accepts:```\n
      >>> str
      >>> list[str]
      >>> tuple[str,str]
      >>> "Can mix & match types"

```Raises:```\n
      Exception:
        "Too few arguments if less than 2 paths are provided."
      Exception:
        "Argument is not in proper format. List or tuple must be filled with strictly strings."

  ```Returns:```\n
      str:
        "All listed paths combined and formatted."
  """
  if len(args) < 2 and (type(args[0]) != list) and (type(args[0]) != tuple):
    raise Exception("Too few arguments")
  
  returnval : str = ""
  current_arg : int = 0
  for arg in args:
    if type(arg) == str:
      returnval = __f_pth_arg_com_(returnval, arg)
    elif type(arg) == list:
      try:
        i = 1; returnval = __f_pth_arg_com_(returnval, arg[0])
        while i < len(list(arg)):
          returnval = __f_pth_arg_com_(returnval, arg[i])
          i+=1
      except:
        raise Exception("The list in argument position " + current_arg + " was not in the correct format, must be string list:\r\n" + arg)
    elif type(arg) == tuple:
      try:
        returnval = __f_pth_arg_com_(__f_pth_arg_com_(returnval, arg[0]), arg[1])
      except:
        raise Exception("The tuple in argument position " + current_arg + " was not in the correct format, tuple must contain strictly strings:\r\n" + arg)
    current_arg+=1
  return returnval

class Scraper:
  init_code   : int  = -1
  
  Updating    : bool = False
  UpdatingDN  : bool = False
  
  TextIO      : TextIOWrapper = None
  
  ListCreator   : ytvl.ListCreator
  """YTD Scraper List Creator Object. Does Scraping & Logging."""
        
  def __init__(self, file : TextIOWrapper = None):
    headless = False
    self.TextIO = file
    if (WEB_DRIVER == 'firefox') or (WEB_DRIVER == 'chrome'):
      headless = RUN_HIDDEN
    start_code = self.__begin__()
    if start_code == 0:
      if os.name == 'nt':
        shutil.copy(f'{WEB_DRIVER_DIR}/{WEB_DRIVER_DICT[WEB_DRIVER]}.exe', f'./{WEB_DRIVER_DICT[WEB_DRIVER]}.exe' )
      else:
        shutil.copy(f'{WEB_DRIVER_DIR}/{WEB_DRIVER_DICT[WEB_DRIVER]}', f'./{WEB_DRIVER_DICT[WEB_DRIVER]}' )
      self.ListCreator = ytvl.ListCreator(
        txt=True,
        csv=False,
        md=False,
        headless=headless,
        driver=WEB_DRIVER,
        scroll_pause_time=1.2
      )
    print(start_code, file=self.TextIO)
    self.init_code = start_code
  
  def __begin__(self):
    if not os.path.exists(WEB_DRIVER_DIR):
      return 21
    elif not os.path.exists(SCRAPER_DIR):
      return 22
    elif not os.path.exists(SUBS_FILE):
      return 23
    elif not os.path.exists(OUTPUT_DIR):
      return 24
    elif not os.path.exists(GENERATION_DIR):
      return 25
    elif (self.__scan_drivers__()).count(WEB_DRIVER) <=0:
      print("Bad driver '" + WEB_DRIVER + "', install the driver in question and add functionality to run.py and make sure the Driver Directory is properly set!", file=self.TextIO)
      return 10
    elif self.__remove_junk__() > 0:
      return 26
    elif self.__move_cached__() > 0:
      return 27
    return 0
  
  def __scan_drivers__(self):
    drivers : list[str] = []
    try:
      if (ytdsettings.MACHINE_OS == 'Darwin'):
        drivers.append('safari')
      for root, dirs, files in os.walk(WEB_DRIVER_DIR):
        for f in files:
          if f.count('geckodriver') > 0:
            if not (drivers.count('firefox') > 0):
              drivers.append('firefox')
          elif f.count('chromedriver') > 0:
            if not (drivers.count('chrome') > 0):
              drivers.append('chrome')
          elif f.count('operadriver') > 0:
            if not (drivers.count('opera') > 0):
              drivers.append('opera')
          elif f.count('bravedriver') > 0:
            if not (drivers.count('brave') > 0):
              drivers.append('brave')
    except:
      print("There was an error in reading files in: " + WEB_DRIVER_DIR, file=self.TextIO)
    return drivers

  def __remove_junk__(self):
    try:
      for root, dirs, files in walklevel('.'):
        for f in files:
          if f.startswith('temp') or f.startswith('partial') or f.startswith('full'):
            if f.endswith('.txt') or f.endswith('.log'):
              os.remove(os.path.join(root,f))
      return 0
    except:
      print("Something went wrong with removal of junk files in: " + SCRAPER_DIR, file=self.TextIO)
      return 1

  def __move_cached__(self, attempts : int = 0, __attempt_limit : int = 3) -> int:
    if (os.path.isdir(GENERATION_DIR)):
      for root, dirs, files in walklevel(GENERATION_DIR):
        for f in files:
            if f.endswith('_list.txt') or f.endswith('_list.log'):
              if (not f.startswith("partial")) or (not f.startswith("full")) or (not f.startswith("temp")):
                print("Moving " + f + " to work-directory (scraper directory): " + SCRAPER_DIR, file=self.TextIO)
                shutil.move(os.path.join(root,f), SCRAPER_DIR)
      return 0
    else:
      if (attempts <= __attempt_limit):
        os.mkdir(GENERATION_DIR)
        return self.__move_cached__(attempts + 1)
      else:
        print("Error in making generation directory: " + GENERATION_DIR, file=self.TextIO)
        print("Attempted " + __attempt_limit + " times to create directory without success.", file=self.TextIO)
    return 1

  def update_list(self, threads : THREADS, sleep_minmax : tuple[int,int] = (MIN_SLEEP_DUR,MAX_SLEEP_DUR), pause_for_when : tuple[int,int] = (FORCE_STOP_CHN,FORCE_STOP_DUR), log_silently : bool = LOG_SILENTLY, run_ids : bool = SPECIAL_UPDATE, run_channels : str = CHANNEL_UPDATE, unsafe : bool = UNSAFE):
    print("", file=self.TextIO)
    print("", file=self.TextIO)
    print("=====================================================================", file=self.TextIO)
    print("Performing Update Check", file=self.TextIO)
    print("", file=self.TextIO)
    print("Update Parameters:", file=self.TextIO)
    print("=====================================================================", file=self.TextIO)
    print("Selected Driver                  - " + WEB_DRIVER, file=self.TextIO)
    print("", file=self.TextIO)
    print("Run Update on Normal Channels    - " + str(run_channels), file=self.TextIO)
    print("Run Update on FixedID Channels   - " + str(run_ids), file=self.TextIO)
    print("Number of Threads                - " + str(threads), file=self.TextIO)
    print("Minimum Sleep Time               - " + str(sleep_minmax[0]), file=self.TextIO)
    print("Maximum Sleep Time               - " + str(sleep_minmax[1]), file=self.TextIO)
    print("Pause Scraper After # Of Cycles  - " + str(pause_for_when[0]), file=self.TextIO)
    print("Pause Scraper For # Of Seconds   - " + str(pause_for_when[1]), file=self.TextIO)
    print("Log Status & Info Silently       - " + str(log_silently), file=self.TextIO)
    print("", file=self.TextIO)
    print("=====================================================================", file=self.TextIO)
    print("Performing Update", file=self.TextIO)

    if run_channels:
      print("Attempting Normal Update...", file=self.TextIO)
      print("", file=self.TextIO)
      print("", file=self.TextIO)
      if unsafe:
        self.ListCreator.create_list_from(
          path_to_channel_urls_file=SUBS_FILE,
          number_of_threads=threads,
          min_sleep=sleep_minmax[0],
          max_sleep=sleep_minmax[1],
          after_n_channels_pause_for_s=pause_for_when,
          log_subthread_status_silently=log_silently,
          log_subthread_info_silently=log_silently
        )
      else:
        try:
          self.ListCreator.create_list_from(
            path_to_channel_urls_file=SUBS_FILE,
            number_of_threads=threads,
            min_sleep=sleep_minmax[0],
            max_sleep=sleep_minmax[1],
            after_n_channels_pause_for_s=pause_for_when,
            log_subthread_status_silently=log_silently,
            log_subthread_info_silently=log_silently
          )
        except:
          print("", file=self.TextIO)
          print("An Error occured during pulling URLS of normal channels.", file=self.TextIO)
          return 1
    else:
      print("Skipping Normal Update...", file=self.TextIO)
    
    if run_ids:
      print("Attempting FixedID Update...", file=self.TextIO)
      print("", file=self.TextIO)
      print("", file=self.TextIO)
      if unsafe:
        subs_file = open(SUBS_FILE, encoding='utf8')
        ids_file  = open(SCRAPER_DIR + "/ids", 'w', encoding="utf8")
  
        id_channels : str = ''
        for l in subs_file:
          if l.count('###') > 0:
            id_channels += l.split('###')[0].rstrip().removeprefix('#') + "\r\n"
        print(id_channels)
        subs_file.close()
  
        ids_file.write(id_channels)
        ids_file.close()
  
        self.ListCreator.create_list_from(
          path_to_channel_urls_file=SCRAPER_DIR + "/ids",
          file_name='id',
          number_of_threads=threads,
          min_sleep=sleep_minmax[0],
          max_sleep=sleep_minmax[1],
          after_n_channels_pause_for_s=pause_for_when,
          log_subthread_status_silently=log_silently,
          log_subthread_info_silently=log_silently
        )
        
        os.remove(SCRAPER_DIR + "/ids")
      else:
        try:
          subs_file = open(SUBS_FILE, encoding='utf8')
          ids_file  = open(SCRAPER_DIR + "/ids", 'w', encoding="utf8")
          print('opened', file=self.TextIO)

          id_channels : str
          for l in subs_file:
            if l.count('###') > 0:
              id_channels += l.split('###')[0].rstrip().removeprefix('#') + "\r\n"
          print(id_channels, file=self.TextIO)
          subs_file.close()

          ids_file.write(id_channels)
          ids_file.close()

          self.ListCreator.create_list_for(
            path_to_channel_urls_file=SCRAPER_DIR + "/ids",
            file_name='id',
            number_of_threads=threads,
            min_sleep=sleep_minmax[0],
            max_sleep=sleep_minmax[1],
            after_n_channels_pause_for_s=pause_for_when,
            log_subthread_status_silently=log_silently,
            log_subthread_info_silently=log_silently
          )

          os.remove(SCRAPER_DIR + "/ids")

        except:
          print("", file=self.TextIO)
          print("An Error occured during pulling URLS of fixed-id channels.", file=self.TextIO)
          return 2
    else:
      print("Skipping FixedID/Special Update...", file=self.TextIO)
    print("", file=self.TextIO)
    print("", file=self.TextIO)
    print("Successfully finished update.", file=self.TextIO)
    print("", file=self.TextIO)
    print("Cleaning up...", file=self.TextIO)
    cleanup = self.__cleanup__(unsafe=unsafe)
    return cleanup

  def __cleanup__(self, unsafe : bool = UNSAFE):
    import shutil
    unsafe = True
    if not unsafe:
      file_in_progress : str = ''
      try:
        for root, dirs, files in walklevel('.'):
          for f in files:
            if f.endswith('_list.log'):
              if (not f.startswith("partial")) or (not f.startswith("full")) or (not f.startswith("temp")):
                  shutil.move(os.path.join(root,f), GENERATION_DIR)
                  print(f'Moved {f} to {GENERATION_DIR}/{f}')
            elif f.endswith('_list.txt'):
              if (not f.startswith("partial")) or (not f.startswith("full")) or (not f.startswith("temp")):
                  print("Copying " + f + " to " + OUTPUT_DIR)
                  shutil.copy(os.path.join(root,f), OUTPUT_DIR)
                  contents = open(f, "r", encoding="utf8")
                  doc_title = f.removesuffix('_reverse_chronological_videos_list.txt').replace("&", "_").replace("?", "_").replace("\\", "_").replace(" ", "_").replace("=", "_")
                  output : str = ""
                  i : int = 0
                  # Iterate over each row in the csv using reader object
                  num : str; title : str; dur : str; linkA : str; link : str
                  for line in contents:
                      if i == 0: 
                          num     = line.removeprefix("Video Number:      ").removesuffix("\r\n").removesuffix("\n")
                          i += 1
                      elif i == 1:
                          title   = line.removeprefix("Video Title:       ").removesuffix("\r\n").removesuffix("\n")
                          i += 1
                      elif i == 2:
                          dur     = line.removeprefix("Video Duration:    ").removesuffix("\r\n").removesuffix("\n")
                          i += 1
                      elif i == 3:
                          linkA    = line.removeprefix("Video URL:         ").removesuffix("\r\n").removesuffix("\n")
                          link     = "/watch?v=" + linkA.split("?v=")[1] + '&sender=' + doc_title + '.info'
                          i += 1
                      elif (i == 4) or (i == 5) or (i == 6):
                          i += 1
                      elif i == 7:
                          title = title.replace("'", "&apos;")
                          title = title.replace("\\", "/")
                          output += "[ '" + num + "', '" + dur + "', '" + title + "', '" + link + "', '" + linkA + "' ], "
                          i = 0
                  contents.close()
                  output.removesuffix(",\r\n")
                  push_dir = OUTPUT_DIR
                  if not (OUTPUT_DIR.endswith("\\")):
                      push_dir = OUTPUT_DIR + "\\"
                  elif not (OUTPUT_DIR.endswith('/')):
                      push_dir = OUTPUT_DIR + '/'

                  newfile = open(push_dir + doc_title + '.info', "w", encoding="utf8")
                  newfile.write(output)
                  newfile.close()
                  os.remove(path_combine(OUTPUT_DIR, f))
                  print("Converted:  " + f + "  ------>  " + doc_title + '.info')
                  shutil.move(os.path.join(root,f), GENERATION_DIR)
                  print(f'Moved {f} to {GENERATION_DIR}/{f}')
      except:
        print("")
        print("Error during cleanup of: " + f)
        return 13
    else:
      for root, dirs, files in walklevel('.'):
        for f in files:
          if f.endswith('_list.log'):
            if (not f.startswith("partial")) or (not f.startswith("full")) or (not f.startswith("temp")):
                shutil.move(os.path.join(root,f), GENERATION_DIR)
                print(f'Moved {f} to {GENERATION_DIR}/{f}')
          elif f.endswith('_list.txt'):
            if (not f.startswith("partial")) or (not f.startswith("full")) or (not f.startswith("temp")):
                print("Copying " + f + " to " + OUTPUT_DIR)
                shutil.copy(os.path.join(root,f), OUTPUT_DIR)
                contents = open(f, "r", encoding="utf8")
                doc_title = f.removesuffix('_reverse_chronological_videos_list.txt').replace("&", "_").replace("?", "_").replace("\\", "_").replace(" ", "_").replace("=", "_")
                output : str = ""
                i : int = 0
                # Iterate over each row in the csv using reader object
                num : str; title : str; dur : str; linkA : str; link : str
                for line in contents:
                    if i == 0: 
                        num     = line.removeprefix("Video Number:      ").removesuffix("\r\n").removesuffix("\n")
                        i += 1
                    elif i == 1:
                        title   = line.removeprefix("Video Title:       ").removesuffix("\r\n").removesuffix("\n")
                        i += 1
                    elif i == 2:
                        dur     = line.removeprefix("Video Duration:    ").removesuffix("\r\n").removesuffix("\n")
                        i += 1
                    elif i == 3:
                        linkA    = line.removeprefix("Video URL:         ").removesuffix("\r\n").removesuffix("\n")
                        link     = "/watch?v=" + linkA.split("?v=")[1] + '&sender=' + doc_title + '.info'
                        i += 1
                    elif (i == 4) or (i == 5) or (i == 6):
                        i += 1
                    elif i == 7:
                        title = title.replace("'", "&apos;")
                        title = title.replace("\\", "/")
                        output += "[ '" + num + "', '" + dur + "', '" + title + "', '" + link + "', '" + linkA + "' ], "
                        i = 0
                contents.close()
                output.removesuffix(",\r\n")
                push_dir = OUTPUT_DIR
                if not (OUTPUT_DIR.endswith("\\")):
                    push_dir = OUTPUT_DIR + "\\"
                elif not (OUTPUT_DIR.endswith('/')):
                    push_dir = OUTPUT_DIR + '/'

                newfile = open(push_dir + doc_title + '.info', "w", encoding="utf8")
                newfile.write(output)
                newfile.close()
                os.remove(path_combine(OUTPUT_DIR, f))
                print("Converted:  " + f + "  ------>  " + doc_title + '.info')
                shutil.move(os.path.join(root,f), GENERATION_DIR)
                print(f'Moved {f} to {GENERATION_DIR}/{f}')
    if os.name == 'nt':
      if os.path.isfile(os.path.abspath('./' + WEB_DRIVER_DICT[WEB_DRIVER]) + '.exe'):
        os.remove('./' + WEB_DRIVER_DICT[WEB_DRIVER] + '.exe')
    else:
      if os.path.isfile(os.path.abspath('./' + WEB_DRIVER_DICT[WEB_DRIVER])):
        os.remove('./' + WEB_DRIVER_DICT[WEB_DRIVER])
    return 0

def getchs() -> list[str]:
    res : list[str] = []
    import os
    for root, dirs, files in os.walk(OUTPUT_DIR):
        for f in files:
            if f.endswith('.info'):
                res.append(f)
    return res

import sys
def repair():
  ytdscraper = Scraper()
  if ytdscraper.init_code == 0:
    return ytdscraper.__cleanup__()
  else:
    return ytdscraper.init_code

def run():
    """Begin Scraping/Updating Channels
    """
    global Scraper, sys
    Scraper.Updating        = True
    start       = millis()
    with open("update.log", 'a') as log:
        print   ( f"[{start}] Beginning Update...", file=log)
        ytd     = Scraper(file=log)
        stCode  = ytd.init_code
        if stCode != 0:
          end   = millis()
          print ( f'[{end}] Failed to initialize Scraper. Returned {stCode}. Took {abs(float(start-end) / 1000.0)} Seconds.')
          Scraper.Updating  = False
          return
        retCode = ytd.update_list(threads=4)
        end     = millis()
        print   ( f"[{end}] Finished Updating. Returned {retCode}. Took {abs(float(start-end) / 1000.0)} Seconds.")
    Scraper.UpdatingDN      = True
    Scraper.Updating        = False
    
def mpvWatchYT(elevenChars : str, unsafe : bool = False) -> int:
  unsafe = True
  if not unsafe:
    try:
        import    re
        charRe  = re.compile(r'[^a-zA-Z0-9-_]')
        videok  = not bool  (charRe.search(elevenChars))
        if (videok) and (len(elevenChars) == 11):
            from subprocess import Popen
            Popen(f'python3.9 ytdapp/mpv.py https://www.youtube.com/watch?v={elevenChars}')
        else:
            print("Error, video string does not meet qualifications.")
            return 2
    except:
        print("Error in loading video.")
        return 1
    return 0
  else:
    import    re
    charRe  = re.compile(r'[^a-zA-Z0-9-_]')
    videok  = not bool  (charRe.search(elevenChars))
    if (videok) and (len(elevenChars) == 11):
      from subprocess import Popen
      #Popen(f'python3.9 ytdapp/mpv.py https://www.youtube.com/watch?v=' + elevenChars)
      import ytdapp._mpv as mpv
      mpv.main(override=True,location=os.path.abspath('ytdapp'),_file="https://www.youtube.com/watch?v=" + elevenChars)
      #os.system(f'python3.9 ytdapp/mpv.py https://www.youtube.com/watch?v=' + elevenChars)
    else:
      print("Error, video string does not meet qualifications.")
      return 2
    return 0

from django.template import Template
def template_from_string(template_string, using=None) -> Template:
    """
    Convert a string into a template object,
    using a given template engine or using the default backends 
    from settings.TEMPLATES if no engine was specified.
    """
    from django.template import engines, TemplateSyntaxError
    # This function is based on django.template.loader.get_template, 
    # but uses Engine.from_string instead of Engine.get_template.
    chain = []
    engine_list = engines.all() if using is None else [engines[using]]
    for engine in engine_list:
        try:
            return engine.from_string(template_string)
        except TemplateSyntaxError as e:
            chain.append(e)
    raise TemplateSyntaxError(template_string, chain=chain)
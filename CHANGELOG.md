# CHANGELOG.md

## 0.3.14 (March 14, 2022)

General:

  - Updated README.md -> [0023f6ff](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)
  - Deobfuscated PHP -> [0023f6ff](https://gitlab.com/vasilaev/yt-divert/-/tree/0023f6ff9080f5afef2746f35849358d7ac070ed)
  - Removed PHP -> [ea3d7a04](https://gitlab.com/vasilaev/yt-divert/-/commit/ea3d7a043b2ac30783cff0aeada103a2852768cb)
  - Forced Django Webserver -> [ea3d7a04](https://gitlab.com/vasilaev/yt-divert/-/commit/ea3d7a043b2ac30783cff0aeada103a2852768cb)
  - New README, Install Sequence, and Dependencies -> [ea3d7a04](https://gitlab.com/vasilaev/yt-divert/-/commit/ea3d7a043b2ac30783cff0aeada103a2852768cb)

Security:

  - Linux Support -> [79893a93](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)
  - remove final scraper batch file -> [79893a93](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)
  - remove MPV executable -> [79893a93](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)
  - tokenize GET and POST data -> [0023f6ff](https://gitlab.com/vasilaev/yt-divert/-/tree/0023f6ff9080f5afef2746f35849358d7ac070ed)
  - Exec & escape string filtering -> [0023f6ff](https://gitlab.com/vasilaev/yt-divert/-/tree/0023f6ff9080f5afef2746f35849358d7ac070ed)
  - Created Login Screen -> [ea3d7a04](https://gitlab.com/vasilaev/yt-divert/-/commit/ea3d7a043b2ac30783cff0aeada103a2852768cb)

Optimization:

  - Removal of yt-videos-list files; added pip dependency requirement -> [79893a93](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)
  - Removal of mpv executable, added mpv to installation in README -> [79893a93](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)
  - Moved all scraper python scripts to `scraper/python` folder -> [79893a93](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)

Features:

  - ~~Added geckodriver for linux -> [79893a93](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)~~ Removed [ea3d7a04](https://gitlab.com/vasilaev/yt-divert/-/commit/ea3d7a043b2ac30783cff0aeada103a2852768cb)
  - ~~Added yt-dlp for linux -> [79893a93](https://gitlab.com/vasilaev/yt-divert/-/tree/79893a939be087ae8bfba5a4cf765be791d5006e)~~ Removed [ea3d7a04](https://gitlab.com/vasilaev/yt-divert/-/commit/ea3d7a043b2ac30783cff0aeada103a2852768cb)
  - New Look -> [ea3d7a04](https://gitlab.com/vasilaev/yt-divert/-/commit/ea3d7a043b2ac30783cff0aeada103a2852768cb)

## 0.3.4 (March 4, 2022)

Features:

  - Windows Only -> [935ab62c](https://gitlab.com/vasilaev/yt-divert/-/tree/935ab62cee47b8588b6b846af3c452206bdea364/)
  - Completed initial pushes -> [935ab62c](https://gitlab.com/vasilaev/yt-divert/-/tree/935ab62cee47b8588b6b846af3c452206bdea364/)
  - Made project live -> [935ab62c](https://gitlab.com/vasilaev/yt-divert/-/tree/935ab62cee47b8588b6b846af3c452206bdea364/)

"""ytdweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from ytdapp import ytd as ytd
from ytdapp.views import *
from django.conf.urls.static import static
from ytdweb import settings
from django.urls import path

urlpatterns = [
    path('', login, name='root'),
    path('login', login, name='login'),
    path('channels', channels, name='channels'),
    path('videos', videos, name='videos'),
    path('repair', repair, name='repair'),
    path('update', update, name='update'),
    path('updating', updating, name='updating'),
    path('watch', watch, name='watch'),
    path('edit', edit, name='edit'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

handler400 = 'ytdapp.views.err400'
handler401 = 'ytdapp.views.err401'
handler403 = 'ytdapp.views.err403'
handler404 = 'ytdapp.views.err404'
handler405 = 'ytdapp.views.err405'
handler406 = 'ytdapp.views.err406'
handler407 = 'ytdapp.views.err407'
handler408 = 'ytdapp.views.err408'
handler409 = 'ytdapp.views.err409'
handler410 = 'ytdapp.views.err410'
handler413 = 'ytdapp.views.err413'
handler414 = 'ytdapp.views.err414'
handler415 = 'ytdapp.views.err415'
handler416 = 'ytdapp.views.err416'
handler417 = 'ytdapp.views.err417'
handler418 = 'ytdapp.views.err418'
handler429 = 'ytdapp.views.err429'
handler500 = 'ytdapp.views.err500'
handler501 = 'ytdapp.views.err501'
handler502 = 'ytdapp.views.err502'
handler503 = 'ytdapp.views.err503'
handler504 = 'ytdapp.views.err504'
handler505 = 'ytdapp.views.err505'
handler510 = 'ytdapp.views.err510'
handler511 = 'ytdapp.views.err511'
